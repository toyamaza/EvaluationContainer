FROM fbarreir/horovod-base:latest

## linux packages for debugging
RUN apt-get -y install rsyslog
RUN apt-get -y install libopenblas-dev
RUN apt-get -y install libomp-dev

##################################################################################
# Below: ATLAS Evaluation Container specific (Rui Zhang, rui.zhang@cern.ch)      #
##################################################################################
ARG PROJECT=ATLASMLHPO
RUN pip uninstall -y torch torchvision torch-scatter torch-cluster torch-spline-conv torch-geometric pytorch-lightning graph_nets faiss
ENV CUDA=cpu
ENV PYTORCH_VERSION=1.6.0

RUN pip install torch==${PYTORCH_VERSION}+${CUDA} torchvision==${TORCHVISION_VERSION}+${CUDA} -f https://download.pytorch.org/whl/torch_stable.html
RUN pip install torch-scatter -f https://pytorch-geometric.com/whl/torch-1.6.0+${CUDA}.html
RUN pip install torch-sparse -f https://pytorch-geometric.com/whl/torch-1.6.0+${CUDA}.html
RUN pip install torch-cluster -f https://pytorch-geometric.com/whl/torch-1.6.0+${CUDA}.html
RUN pip install torch-spline-conv -f https://pytorch-geometric.com/whl/torch-1.6.0+${CUDA}.html
RUN pip install torch-geometric pytorch-lightning graph_nets faiss

RUN apt-get update && \
    apt-get install -y --no-install-recommends graphviz libopenblas-dev libomp-dev && \
    apt-get clean
COPY ./payload/DLGNN4Tracking /$PROJECT/payload/DLGNN4Tracking
RUN pip install mpi4py
RUN pip install /$PROJECT/payload/DLGNN4Tracking/exatrkx-iml2020

##################################################################################
# Below: ssh key (Taking from Fernando, fernando.harald.barreiro.megino@cern.ch) #
# https://github.com/fbarreir/horovod-kube/blob/main/basic_test/Dockerfile       #
##################################################################################

# ADD ssh/id_rsa.pub /root/.ssh/authorized_keys
# ADD ssh/id_rsa.pub /root/.ssh/id_rsa.pub
# ADD ssh/id_rsa /root/.ssh/id_rsa
# RUN chmod 700 /root/.ssh
# RUN chmod 600 /root/.ssh/authorized_keys
# RUN chmod 600 /root/.ssh/id_rsa.pub
# RUN chmod 600 /root/.ssh/id_rsa

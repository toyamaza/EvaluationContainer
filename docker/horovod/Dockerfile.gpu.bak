##################################################################################
# Below: taken from https://horovod.readthedocs.io/en/stable/docker_include.html #
##################################################################################
FROM nvidia/cuda:10.1-devel-ubuntu18.04

# TensorFlow version is tightly coupled to CUDA and cuDNN so it should be selected carefully
ENV TENSORFLOW_VERSION=2.3.0
ENV PYTORCH_VERSION=1.6.0
ENV TORCHVISION_VERSION=0.7.0
ENV CUDNN_VERSION=7.6.5.32-1+cuda10.1
ENV NCCL_VERSION=2.7.8-1+cuda10.1
ENV MXNET_VERSION=1.6.0.post0

# Python 3.7 is supported by Ubuntu Bionic out of the box
ARG python=3.7
ENV PYTHON_VERSION=${python}

# Set default shell to /bin/bash
SHELL ["/bin/bash", "-cu"]

RUN apt-get update && apt-get install -y --allow-downgrades --allow-change-held-packages --no-install-recommends \
        build-essential \
        cmake \
        g++-7 \
        git \
        curl \
        vim \
        wget \
        ca-certificates \
        libcudnn7=${CUDNN_VERSION} \
        libnccl2=${NCCL_VERSION} \
        libnccl-dev=${NCCL_VERSION} \
        libjpeg-dev \
        libpng-dev \
        python${PYTHON_VERSION} \
        python${PYTHON_VERSION}-dev \
        python${PYTHON_VERSION}-distutils \
        librdmacm1 \
        libibverbs1 \
        ibverbs-providers

RUN ln -s /usr/bin/python${PYTHON_VERSION} /usr/bin/python

RUN curl -O https://bootstrap.pypa.io/get-pip.py && \
    python get-pip.py && \
    rm get-pip.py

# Install TensorFlow, Keras, PyTorch and MXNet
RUN pip install future typing packaging
RUN pip install tensorflow==${TENSORFLOW_VERSION} \
                keras \
                h5py

# RUN pip install torch==${PYTORCH_VERSION} torchvision==${TORCHVISION_VERSION}
ENV CUDA=cu101
RUN pip install torch==${PYTORCH_VERSION}+${CUDA} torchvision==${TORCHVISION_VERSION}+${CUDA} -f https://download.pytorch.org/whl/torch_stable.html
RUN pip install torch-scatter -f https://pytorch-geometric.com/whl/torch-1.6.0+${CUDA}.html
RUN pip install torch-sparse -f https://pytorch-geometric.com/whl/torch-1.6.0+${CUDA}.html
RUN pip install torch-cluster -f https://pytorch-geometric.com/whl/torch-1.6.0+${CUDA}.html
RUN pip install torch-spline-conv -f https://pytorch-geometric.com/whl/torch-1.6.0+${CUDA}.html
RUN pip install torch-geometric pytorch-lightning graph_nets faiss-gpu==1.6.4
# pytorch-lightning      1.2.0
# torch                  1.6.0+cu101
# torch-cluster          1.5.8
# torch-geometric        1.6.3
# torch-scatter          2.0.5
# torch-sparse           0.6.8
# torch-spline-conv      1.2.0
# torchvision            0.7.0+cu101
RUN PYTAGS=$(python -c "from packaging import tags; tag = list(tags.sys_tags())[0]; print(f'{tag.interpreter}-{tag.abi}')") && \
    pip install https://download.pytorch.org/whl/cu101/torch-${PYTORCH_VERSION}%2Bcu101-${PYTAGS}-linux_x86_64.whl \
        https://download.pytorch.org/whl/cu101/torchvision-${TORCHVISION_VERSION}%2Bcu101-${PYTAGS}-linux_x86_64.whl

RUN pip install mxnet-cu101==${MXNET_VERSION}

# Install Open MPI
RUN mkdir /tmp/openmpi && \
    cd /tmp/openmpi && \
    wget https://www.open-mpi.org/software/ompi/v4.0/downloads/openmpi-4.0.0.tar.gz && \
    tar zxf openmpi-4.0.0.tar.gz && \
    cd openmpi-4.0.0 && \
    ./configure --enable-orterun-prefix-by-default && \
    make -j $(nproc) all && \
    make install && \
    ldconfig && \
    rm -rf /tmp/openmpi

# Install Horovod, temporarily using CUDA stubs
RUN ldconfig /usr/local/cuda/targets/x86_64-linux/lib/stubs && \
    HOROVOD_GPU_OPERATIONS=NCCL HOROVOD_WITH_TENSORFLOW=1 HOROVOD_WITH_PYTORCH=1 HOROVOD_WITH_MXNET=1 \
         pip install --no-cache-dir horovod && \
    ldconfig

# Install OpenSSH for MPI to communicate between containers
RUN apt-get install -y --no-install-recommends openssh-client openssh-server && \
    mkdir -p /var/run/sshd

# Allow OpenSSH to talk to containers without asking for confirmation
RUN cat /etc/ssh/ssh_config | grep -v StrictHostKeyChecking > /etc/ssh/ssh_config.new && \
    echo "    StrictHostKeyChecking no" >> /etc/ssh/ssh_config.new && \
    mv /etc/ssh/ssh_config.new /etc/ssh/ssh_config

# # Download examples
# RUN apt-get install -y --no-install-recommends subversion && \
#     svn checkout https://github.com/horovod/horovod/trunk/examples && \
#     rm -rf /examples/.svn

# WORKDIR "/examples"

##################################################################################
# Below: ATLAS Evaluation Container specific (Rui Zhang, rui.zhang@cern.ch)      #
##################################################################################
ARG PROJECT=ATLASMLHPO

RUN apt-get update && \
    apt-get install -y --no-install-recommends  wget unzip bzip2 graphviz libopenblas-dev libomp-dev && \
    apt-get clean
COPY . /$PROJECT
RUN pip install mpi4py
RUN pip install /$PROJECT/payload/DLGNN4Tracking/exatrkx-iml2020

#ARG MLFLOW_VERSION=1.7.2
ENV MLFLOW_SERVER_PORT=5000
ENV MLFLOW_SERVER_HOST=0.0.0.0

#RUN pip install mlflow==${MLFLOW_VERSION}
ENV PATH /$PROJECT/docker/mlflow/:$PATH
ENV PYTHONPATH /$PROJECT/module/:$PYTHONPATH
RUN chmod +x /$PROJECT/docker/mlflow/mlflowUI.sh

EXPOSE ${MLFLOW_SERVER_PORT}/tcp

##################################################################################
# Below: ssh key (Taking from Fernando, fernando.harald.barreiro.megino@cern.ch) #
# https://github.com/fbarreir/horovod-kube/blob/main/basic_test/Dockerfile       #
##################################################################################

# ADD ssh/id_rsa.pub /root/.ssh/authorized_keys
# ADD ssh/id_rsa.pub /root/.ssh/id_rsa.pub
# ADD ssh/id_rsa /root/.ssh/id_rsa
# RUN chmod 700 /root/.ssh
# RUN chmod 600 /root/.ssh/authorized_keys
# RUN chmod 600 /root/.ssh/id_rsa.pub
# RUN chmod 600 /root/.ssh/id_rsa
